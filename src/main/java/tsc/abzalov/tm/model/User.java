package tsc.abzalov.tm.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.enumeration.Role;

import java.util.Optional;

import static tsc.abzalov.tm.util.LiteralConst.*;
import static tsc.abzalov.tm.enumeration.Role.USER;

@Data
@EqualsAndHashCode(callSuper = true)
public final class User extends AbstractEntity {

    @Nullable
    private String login;

    @Nullable
    private String hashedPassword;

    @NotNull
    private Role role = USER;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String email;

    private boolean isLocked = false;

    @Override
    @NotNull
    public String toString() {
        @NotNull val correctLogin = Optional.ofNullable(login).orElse(DEFAULT_LOGIN);
        @NotNull val correctFirstName = Optional.ofNullable(login).orElse(DEFAULT_FIRSTNAME);
        @NotNull val correctLastName = Optional.ofNullable(lastName).orElse(DEFAULT_LASTNAME);
        @NotNull val correctEmail = Optional.ofNullable(login).orElse(DEFAULT_EMAIL);
        @NotNull val correctUserStatus = (this.isLocked) ? LOCKED : ACTIVE;
        return "[ID: " + getId() +
                "; Login: " + correctLogin +
                "; Role: " + this.role.getDisplayName() +
                "; First Name: " + correctFirstName +
                "; Last Name: " + correctLastName +
                "; Email: " + correctEmail +
                "; Status: " + correctUserStatus + "]";
    }

}
