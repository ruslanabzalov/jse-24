package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.api.IBusinessEntityService;
import tsc.abzalov.tm.model.Project;

public interface IProjectService extends IBusinessEntityService<Project> {
}
