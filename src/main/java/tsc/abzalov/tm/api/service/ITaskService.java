package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.api.IBusinessEntityService;
import tsc.abzalov.tm.model.Task;

public interface ITaskService extends IBusinessEntityService<Task> {
}
