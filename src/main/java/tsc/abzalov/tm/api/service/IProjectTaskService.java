package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    int indexOf(@NotNull String userId, @NotNull Task task);

    boolean hasData(@NotNull String userId);

    void addTaskToProjectById(
            @NotNull String userId, @NotNull String projectId, @NotNull String taskId
    );

    @Nullable
    Project findProjectById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task findTaskById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<Task> findProjectTasksById(@NotNull String userId, @NotNull String projectId);

    void deleteProjectById(@NotNull String userId, @NotNull String id);

    void deleteProjectTasksById(@NotNull String userId, @NotNull String projectId);

    void deleteProjectTaskById(@NotNull String userId, @NotNull String projectId);

}
