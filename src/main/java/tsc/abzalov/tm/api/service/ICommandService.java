package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    void initCommands(@NotNull IServiceLocator serviceLocator);

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<String> getCommandArguments();

    @NotNull
    AbstractCommand getCommandByName(@NotNull String name);

    @NotNull
    AbstractCommand getArgumentByName(@NotNull String name);

}
