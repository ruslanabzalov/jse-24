package tsc.abzalov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.BusinessEntity;

import java.util.List;

public interface IBusinessEntityService<T extends BusinessEntity> extends IService<T> {

    int size(@NotNull String userId);

    boolean isEmpty(@NotNull String userId);

    int indexOf(@NotNull String userId, @NotNull T entity);

    @NotNull
    List<T> findAll(@NotNull String userId);

    @Nullable
    T findById(@NotNull String userId, @NotNull String id);

    @Nullable
    T findByIndex(@NotNull String userId, int index);

    @Nullable
    T findByName(@NotNull String userId, @NotNull String name);

    @Nullable
    T editById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description);

    @Nullable
    T editByIndex(@NotNull String userId, int index, @NotNull String name, @NotNull String description);

    @Nullable
    T editByName(@NotNull String userId, @NotNull String name, @NotNull String description);

    void clear(@NotNull String userId);

    void removeById(@NotNull String userId, @NotNull String id);

    void removeByIndex(@NotNull String userId, int index);

    void removeByName(@NotNull String userId, @NotNull String name);

    @Nullable
    T startById(@NotNull String userId, @NotNull String id);

    @Nullable
    T endById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<T> sortByName(@NotNull String userId);

    @NotNull
    List<T> sortByStartDate(@NotNull String userId);

    @NotNull
    List<T> sortByEndDate(@NotNull String userId);

    @NotNull
    List<T> sortByStatus(@NotNull String userId);

}
