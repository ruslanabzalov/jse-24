package tsc.abzalov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void initCommands(@NotNull IServiceLocator serviceLocator);

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<String> getCommandArguments();

    @NotNull
    AbstractCommand getCommandByName(@NotNull String name);

    @NotNull
    AbstractCommand getArgumentByName(@NotNull String name);

}
