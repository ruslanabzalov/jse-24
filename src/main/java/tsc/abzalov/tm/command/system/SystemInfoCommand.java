package tsc.abzalov.tm.command.system;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.SYSTEM_COMMAND;
import static tsc.abzalov.tm.util.Formatter.formatBytes;

public final class SystemInfoCommand extends AbstractCommand {

    public SystemInfoCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "info";
    }

    @Override
    @NotNull
    public String getCommandArgument() {
        return "-i";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Shows CPU and RAM info.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return SYSTEM_COMMAND;
    }

    @Override
    public void execute() {
        val cores = Runtime.getRuntime().availableProcessors();
        System.out.println("Cores: " + cores);
        val maxMemory = Runtime.getRuntime().maxMemory();
        System.out.println("Max Memory: " + formatBytes(maxMemory));
        val totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total Memory: " + formatBytes(totalMemory));
        val freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free Memory: " + formatBytes(freeMemory));
        val usedMemory = totalMemory - freeMemory;
        System.out.println("Used Memory: " + formatBytes(usedMemory) + "\n");
    }

}
