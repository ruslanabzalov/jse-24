package tsc.abzalov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.SYSTEM_COMMAND;

public final class SystemAboutCommand extends AbstractCommand {

    public SystemAboutCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "about";
    }

    @Override
    @NotNull
    public String getCommandArgument() {
        return "-a";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Shows developer info";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return SYSTEM_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("Developer Full Name: Ruslan Abzalov");
        System.out.println("Developer Email: rabzalov@tsconsulting.com\n");
    }

}
