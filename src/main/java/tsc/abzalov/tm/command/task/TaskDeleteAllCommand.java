package tsc.abzalov.tm.command.task;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;

public final class TaskDeleteAllCommand extends AbstractCommand {

    public TaskDeleteAllCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "delete-all-tasks";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Delete all tasks.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("TASKS DELETION\n");
        @NotNull val taskService = getServiceLocator().getTaskService();
        @NotNull val currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        val areTasksExist = taskService.size(currentUserId) != 0;
        if (areTasksExist) {
            taskService.clear(currentUserId);
            System.out.println("Tasks were deleted.\n");
            return;
        }
        System.out.println("Tasks list is empty.\n");
    }

}
