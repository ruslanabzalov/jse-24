package tsc.abzalov.tm.command.task;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.*;

public final class TaskUpdateByIdCommand extends AbstractCommand {

    public TaskUpdateByIdCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "update-task-by-id";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Update task by id.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("EDIT TASK BY ID\n");
        @NotNull val taskService = getServiceLocator().getTaskService();
        @NotNull val currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        val areTasksExist = taskService.size(currentUserId) != 0;
        if (areTasksExist) {
            @NotNull val taskId = inputId();
            @NotNull val taskName = inputName();
            @NotNull val taskDescription = inputDescription();
            System.out.println();
            @Nullable val task = taskService.editById(currentUserId, taskId, taskName, taskDescription);
            if (!Optional.ofNullable(task).isPresent()) {
                System.out.println("Task was not updated! Please, check that task exists and try again.");
                return;
            }
            System.out.println("Task was successfully updated.");
            return;
        }
        System.out.println("Tasks list is empty.\n");
    }

}
