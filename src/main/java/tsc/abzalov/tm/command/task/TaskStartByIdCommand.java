package tsc.abzalov.tm.command.task;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputId;

public final class TaskStartByIdCommand extends AbstractCommand {

    public TaskStartByIdCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "start-task-by-id";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Start task by id.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("START TASK BY ID\n");
        @NotNull val taskService = getServiceLocator().getTaskService();
        @NotNull val currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        val areTasksExist = taskService.size(currentUserId) != 0;
        if (areTasksExist) {
            @Nullable val task = taskService.startById(currentUserId, inputId());
            if (!Optional.ofNullable(task).isPresent()) {
                System.out.println("Task was not started! Please, check that task exists or it has correct status.\n");
                return;
            }
            System.out.println("Task was successfully started.\n");
            return;
        }
        System.out.println("Tasks list is empty.\n");
    }

}
