package tsc.abzalov.tm.command.sorting;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.model.Task;

import java.util.List;

import static tsc.abzalov.tm.enumeration.CommandType.SORTING_COMMAND;

public final class SortingTasksByStartDateCommand extends AbstractCommand {

    public SortingTasksByStartDateCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "sort-tasks-by-start-date";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Sort tasks by start date.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return SORTING_COMMAND;
    }

    @Override
    @SuppressWarnings("DuplicatedCode")
    public void execute() {
        System.out.println("ALL TASKS LIST SORTED BY START DATE\n");
        @NotNull val taskService = getServiceLocator().getTaskService();
        @NotNull val currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        val areTasksExist = taskService.size(currentUserId) != 0;
        if (areTasksExist) {
            @NotNull val tasks = taskService.sortByStartDate(currentUserId);
            for (@NotNull val task : tasks)
                System.out.println((taskService.indexOf(currentUserId, task) + 1) + ". " + task);
            System.out.println();
            return;
        }
        System.out.println("Tasks list is empty.\n");
    }

}
