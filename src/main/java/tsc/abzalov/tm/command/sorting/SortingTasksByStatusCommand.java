package tsc.abzalov.tm.command.sorting;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.SORTING_COMMAND;

public final class SortingTasksByStatusCommand extends AbstractCommand {

    public SortingTasksByStatusCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "sort-tasks-by-status";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Sort tasks by status.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return SORTING_COMMAND;
    }

    @Override
    @SuppressWarnings("DuplicatedCode")
    public void execute() {
        System.out.println("ALL TASKS LIST SORTED BY STATUS\n");
        @NotNull val taskService = getServiceLocator().getTaskService();
        @NotNull val currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        val areTasksExist = taskService.size(currentUserId) != 0;
        if (areTasksExist) {
            @NotNull val tasks = taskService.sortByStatus(currentUserId);
            for (@NotNull val task : tasks)
                System.out.println((taskService.indexOf(currentUserId, task) + 1) + ". " + task);
            System.out.println();
            return;
        }
        System.out.println("Tasks list is empty.\n");
    }

}
