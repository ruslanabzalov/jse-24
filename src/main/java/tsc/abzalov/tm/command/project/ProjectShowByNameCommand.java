package tsc.abzalov.tm.command.project;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputName;

public final class ProjectShowByNameCommand extends AbstractCommand {

    public ProjectShowByNameCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "show-project-by-name";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show project by name.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("FIND PROJECT BY NAME\n");
        @NotNull val projectService = getServiceLocator().getProjectService();
        @NotNull val currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        val areProjectsExist = projectService.size(currentUserId) != 0;
        if (areProjectsExist) {
            @NotNull val projectName = inputName();
            System.out.println();
            @Nullable val project = projectService.findByName(currentUserId, projectName);
            if (!Optional.ofNullable(project).isPresent()) {
                System.out.println("Searched project was not found.\n");
                return;
            }
            System.out.println((projectService.indexOf(currentUserId, project) + 1) + ". " + project + "\n");
            return;
        }
        System.out.println("Projects list is empty.\n");
    }

}
