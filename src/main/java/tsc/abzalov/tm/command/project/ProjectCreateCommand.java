package tsc.abzalov.tm.command.project;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.model.Project;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputDescription;
import static tsc.abzalov.tm.util.InputUtil.inputName;

public final class ProjectCreateCommand extends AbstractCommand {

    public ProjectCreateCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "create-project";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create project.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("PROJECT CREATION\n");
        @NotNull val projectName = inputName();
        @NotNull val projectDescription = inputDescription();
        @NotNull val currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        @NotNull val project = new Project();
        project.setName(projectName);
        project.setDescription(projectDescription);
        project.setUserId(currentUserId);
        getServiceLocator().getProjectService().create(project);
        System.out.println("Project was successfully created.\n");
    }

}
