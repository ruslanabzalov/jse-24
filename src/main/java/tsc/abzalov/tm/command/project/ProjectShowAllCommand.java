package tsc.abzalov.tm.command.project;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;

public final class ProjectShowAllCommand extends AbstractCommand {

    public ProjectShowAllCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "show-all-projects";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    @SuppressWarnings("DuplicatedCode")
    public void execute() {
        System.out.println("ALL PROJECTS LIST\n");
        @NotNull val projectService = getServiceLocator().getProjectService();
        @NotNull val currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        val areProjectsExist = projectService.size(currentUserId) != 0;
        if (areProjectsExist) {
            @NotNull val projects = projectService.findAll(currentUserId);
            for (@NotNull val project : projects)
                System.out.println((projectService.indexOf(currentUserId, project) + 1) + ". " + project);
            System.out.println();
            return;
        }
        System.out.println("Projects list is empty.\n");
    }

}
