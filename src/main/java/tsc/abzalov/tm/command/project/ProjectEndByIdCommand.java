package tsc.abzalov.tm.command.project;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputId;

public final class ProjectEndByIdCommand extends AbstractCommand {

    public ProjectEndByIdCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "end-project-by-id";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "End project by id.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("END PROJECT BY ID\n");
        @NotNull val projectService = getServiceLocator().getProjectService();
        @NotNull val currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        val areProjectsExist = projectService.size(currentUserId) != 0;
        if (areProjectsExist) {
            @Nullable val project = projectService.endById(currentUserId, inputId());
            if (!Optional.ofNullable(project).isPresent()) {
                System.out.println("Project was not ended! Please, check that project exists or it has correct status.\n");
                return;
            }
            System.out.println("Project was successfully ended.\n");
            return;
        }
        System.out.println("Projects list is empty.\n");
    }

}
