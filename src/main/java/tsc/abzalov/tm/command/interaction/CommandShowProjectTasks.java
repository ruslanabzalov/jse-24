package tsc.abzalov.tm.command.interaction;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.util.InputUtil;

import static tsc.abzalov.tm.enumeration.CommandType.INTERACTION_COMMAND;

public final class CommandShowProjectTasks extends AbstractCommand {

    public CommandShowProjectTasks(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "show-project-tasks";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show project tasks.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return INTERACTION_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("SHOW PROJECT TASKS\n");
        @NotNull val projectTasksService = getServiceLocator().getProjectTaskService();
        @NotNull val currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        if (projectTasksService.hasData(currentUserId)) {
            System.out.println("Project");
            @NotNull val projectId = InputUtil.inputId();
            System.out.println();
            @NotNull val tasks = projectTasksService.findProjectTasksById(projectId, currentUserId);
            if (tasks.size() == 0) {
                System.out.println("Tasks list is empty.\n");
                return;
            }
            for (@NotNull val task : tasks)
                System.out.println((projectTasksService.indexOf(currentUserId, task) + 1) + ". " + task);
            System.out.println();
            return;
        }
        System.out.println("One of the lists is empty!");
    }

}
