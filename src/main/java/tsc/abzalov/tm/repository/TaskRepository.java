package tsc.abzalov.tm.repository;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.model.Task;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractBusinessEntityRepository<Task> implements ITaskRepository {

    @NotNull
    private final List<Task> tasks = this.findAll();

    @Override
    public void addTaskToProjectById(
            @NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId
    ) {
        tasks.stream()
                .filter(task -> userId.equals(task.getUserId()) && taskId.equals(task.getId()))
                .findFirst()
                .map(task -> {
                    task.setProjectId(projectId);
                    return task;
                });
    }

    @Override
    @NotNull
    public List<Task> findProjectTasksById(@NotNull final String userId, @NotNull final String projectId) {
        return tasks.stream()
                .filter(task -> userId.equals(task.getUserId()) && isProjectTaskExist(projectId, task))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteProjectTasksById(@NotNull final String userId, @NotNull final String projectId) {
        tasks.removeAll(findProjectTasksById(userId, projectId));
    }

    @Override
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void deleteProjectTaskById(@NotNull final String userId, @NotNull final String projectId) {
        tasks.stream()
                .filter(task -> userId.equals(task.getUserId()) && projectId.equals(task.getProjectId()))
                .peek(task -> task.setProjectId(null));
    }

    private boolean isProjectTaskExist(@NotNull final String projectId, @NotNull final Task task) {
        @Nullable val currentProjectId = task.getProjectId();
        if (Optional.ofNullable(currentProjectId).isPresent()) return projectId.equals(currentProjectId);
        return false;
    }

}
