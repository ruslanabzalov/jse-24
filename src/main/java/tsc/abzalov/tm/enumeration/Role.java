package tsc.abzalov.tm.enumeration;

import org.jetbrains.annotations.NotNull;

public enum Role {

    ADMIN("Administrator"),
    USER("User");

    @NotNull
    private final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
