package tsc.abzalov.tm.enumeration;

import org.jetbrains.annotations.NotNull;

public enum CommandType {

    SYSTEM_COMMAND("System Command"),
    PROJECT_COMMAND("Project Command"),
    TASK_COMMAND("Task Command"),
    INTERACTION_COMMAND("Interaction Command"),
    SORTING_COMMAND("Sorting Command"),
    AUTH_COMMAND("Auth Command"),
    USER_COMMAND("User Command"),
    ADMIN_COMMAND("Admin Command");

    @NotNull
    private final String displayName;

    CommandType(@NotNull final String name) {
        this.displayName = name;
    }

    @NotNull
    public String getDisplayName() {
        return this.displayName;
    }

}
