package tsc.abzalov.tm.exception.auth;

import tsc.abzalov.tm.exception.AbstractException;

public final class CannotLockCurrentUserException extends AbstractException {

    public CannotLockCurrentUserException() {
        super("Cannot lock current user!");
    }

}
