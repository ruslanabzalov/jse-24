package tsc.abzalov.tm.exception.data;

import tsc.abzalov.tm.exception.AbstractException;

public final class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Name is empty!");
    }

}
