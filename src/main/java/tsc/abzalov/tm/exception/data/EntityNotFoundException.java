package tsc.abzalov.tm.exception.data;

import tsc.abzalov.tm.exception.AbstractException;

public final class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Entity is not found!");
    }

}
