package tsc.abzalov.tm.bootstrap;

import lombok.Getter;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.api.service.*;
import tsc.abzalov.tm.exception.auth.AccessDeniedException;
import tsc.abzalov.tm.exception.auth.UserIsNotExistException;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.repository.CommandRepository;
import tsc.abzalov.tm.repository.ProjectRepository;
import tsc.abzalov.tm.repository.TaskRepository;
import tsc.abzalov.tm.repository.UserRepository;
import tsc.abzalov.tm.service.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static tsc.abzalov.tm.enumeration.Role.ADMIN;
import static tsc.abzalov.tm.util.InputUtil.INPUT;

/**
 * Bootstrap application class.
 * @author Ruslan Abzalov.
 */
@Getter
public final class CommandBootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    public CommandBootstrap() {
        // Commands registration.
        registerCommands();
    }

    // Default users initialization.
    {
        try {
            userService.create("admin", "admin", ADMIN, "Admin", "", "admin@mail.com");

            @Nullable val admin = userService.findByLogin("admin");
            Optional.ofNullable(admin).orElseThrow(UserIsNotExistException::new);

            initProject("Main Project", "Main Admin Project", admin.getId());
            initTask("Main Task", "Main Admin Task", admin.getId());

            userService.create("test", "test", "Test", "", "test@mail.com");

            @Nullable val testUser = userService.findByLogin("test");
            Optional.ofNullable(testUser).orElseThrow(UserIsNotExistException::new);

            initProject("Simple Project", "Simple Test User Project", testUser.getId());
            initTask("Simple Task", "Simple Test User Task", testUser.getId());
        } catch (Exception exception) {
            loggerService.error(exception);
        }
    }

    /**
     * Application main method with infinite loop.
     * @param args Commandline arguments.
     * @author Ruslan Abzalov.
     */
    public void run(@NotNull final String... args) {
        System.out.println();
        if (areArgsExecuted(args)) return;

        loggerService.info("****** WELCOME TO TASK MANAGER APPLICATION ******");
        System.out.println("Please, use \"help\" command to see all available commands.\n");
        System.out.println("Please, login/register new user to start work with the application.\n");

        @NotNull val availableStartupCommands = Arrays.asList("register", "login", "logoff", "help", "exit");
        @Nullable String commandName;
        //noinspection InfiniteLoopStatement
        while (true) {
            System.out.print("Please, enter your command: ");
            commandName = INPUT.nextLine();
            System.out.println();

            if (isEmpty(commandName)) continue;

            if (authService.isSessionInactive()) {
                inactiveSessionExecution(availableStartupCommands, commandName);
                continue;
            }

            activeSessionExecution(commandName);
        }
    }

    private boolean areArgsExecuted(@NotNull final String[] args) {
        try {
            if (areArgExists(args)) return true;
        } catch (@NotNull final Exception exception) {
            loggerService.error(exception);
            return true;
        }
        return false;
    }

    private void inactiveSessionExecution(
            @NotNull final List<String> availableStartupCommands,
            @NotNull final String commandName
    ) {
        val isCommandAvailable = availableStartupCommands.contains(commandName);

        if (!isCommandAvailable) {
            System.out.println("Session is inactive! Please, register new user or login.");
            return;
        }

        try {
            loggerService.command(commandName);
            @NotNull val command = commandService.getCommandByName(commandName);
            command.execute();
        } catch (@NotNull final Exception exception) {
            loggerService.error(exception);
        }
    }

    private void activeSessionExecution(@NotNull final String commandName) {
        try {
            @NotNull val command = commandService.getCommandByName(commandName);
            @NotNull val commandRoles = command.getRoles();
            @NotNull val currentUserRole = authService.getCurrentUserRole();
            val canUserExecuteCommand = commandRoles.contains(currentUserRole);

            if (canUserExecuteCommand) {
                loggerService.command(commandName);
                command.execute();
                return;
            }

            throw new AccessDeniedException();
        } catch (@NotNull final Exception exception) {
            loggerService.error(exception);
        }
    }

    /**
     * Register commands method.
     * @author Ruslan Abzalov.
     */
    private void registerCommands() {
        commandService.initCommands(this);
    }

    /**
     * Project initialization method.
     * @param projectName Project name.
     * @param projectDescription Project description.
     * @param userId Project user id.
     * @author Ruslan Abzalov.
     */
    private void initProject(
            @NotNull final String projectName, @NotNull final String projectDescription, @NotNull final String userId
    ) {
        @NotNull val project = new Project();
        project.setName(projectName);
        project.setDescription(projectDescription);
        project.setUserId(userId);
        projectService.create(project);
    }

    /**
     * Task initialization method.
     * @param taskName Task name.
     * @param taskDescription Task description.
     * @param userId Task user id.
     */
    private void initTask(
            @NotNull final String taskName, @NotNull final String taskDescription, @NotNull final String userId
    ) {
        @NotNull val task = new Task();
        task.setName(taskName);
        task.setDescription(taskDescription);
        task.setUserId(userId);
        taskService.create(task);
    }

    /**
     * Execute command if commandline arguments exist.
     * @param args Commandline arguments.
     * @return Was command executed.
     */
    private boolean areArgExists(@NotNull final String... args) {
        if (isEmpty(args)) return false;
        @NotNull val arg = args[0];
        if (isEmpty(arg)) return false;
        commandService.getArgumentByName(arg).execute();
        return true;
    }

}
