package tsc.abzalov.tm.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class LiteralConst {

    public static final String DEFAULT_NAME = "Name is empty";

    public static final String DEFAULT_DESCRIPTION = "Description is empty";

    public static final String IS_NOT_STARTED = "Is not started";

    public static final String IS_NOT_ENDED = "Is not ended";

    public static final String DEFAULT_REFERENCE = "Empty";

    public static final String DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm:ss";

    public static final String DEFAULT_LOGIN = "Login is empty";

    public static final String DEFAULT_FIRSTNAME = "Firstname is empty";

    public static final String DEFAULT_LASTNAME = "Lastname is empty";

    public static final String DEFAULT_EMAIL = "Email is empty";

    public static final String ACTIVE = "Active";

    public static final String LOCKED = "Locked";

    public static final String COMMANDS_PACKAGE = "tsc.abzalov.tm.command";

}
