package tsc.abzalov.tm.service;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.*;

import static java.util.logging.Logger.getGlobal;
import static java.util.logging.Logger.getLogger;

@SuppressWarnings("ConstantConditions")
public final class LoggerService implements ILoggerService {

    private static final String MESSAGES = "MESSAGES";

    private static final String COMMANDS = "COMMANDS";

    private static final String ERRORS = "ERRORS";

    private static final String COMMANDS_LOG_FILE_PATH = "logs/access.log";

    private static final String ERRORS_LOG_FILE_PATH = "logs/errors.log";

    @NotNull
    private final Logger messages = getLogger(MESSAGES);

    @NotNull
    private final Logger commands = getLogger(COMMANDS);

    @NotNull
    private final Logger error = getLogger(ERRORS);

    @NotNull
    private final Logger root = getGlobal();

    public LoggerService() {
        initLogger(messages, null);
        initLogger(commands, COMMANDS_LOG_FILE_PATH);
        initLogger(error, ERRORS_LOG_FILE_PATH);
    }

    @Override
    public void info(@NotNull final String message) {
        if (Optional.of(message).isPresent()) messages.info(message);
    }

    @Override
    public void command(@NotNull final String cmd) {
        if (Optional.of(cmd).isPresent()) commands.info("Execution of command \"" + cmd + "\"");
    }

    @Override
    public void error(@NotNull final Exception exception) {
        error.log(Level.SEVERE, exception.getMessage(), exception);
    }

    private void initLogger(@NotNull final Logger logger, @Nullable final String filePath) {
        logger.setUseParentHandlers(false);
        logger.addHandler(getConsoleHandler());
        if (Optional.ofNullable(filePath).isPresent()) {
            try {
                logger.addHandler(new FileHandler(filePath));
            } catch (IOException exception) {
                root.severe(exception.getMessage());
            }
        }
    }

    @NotNull
    private Handler getConsoleHandler() {
        @NotNull val consoleHandler = new ConsoleHandler();
        consoleHandler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return consoleHandler;
    }

}
