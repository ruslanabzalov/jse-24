package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.IBusinessEntityRepository;
import tsc.abzalov.tm.api.IBusinessEntityService;
import tsc.abzalov.tm.exception.data.*;
import tsc.abzalov.tm.model.BusinessEntity;

import java.util.List;
import java.util.Optional;

import static org.apache.commons.lang3.ObjectUtils.anyNull;
import static tsc.abzalov.tm.util.LiteralConst.DEFAULT_DESCRIPTION;
import static tsc.abzalov.tm.util.InputUtil.isIndexIncorrect;

@SuppressWarnings("ResultOfMethodCallIgnored")
public abstract class AbstractBusinessEntityService<T extends BusinessEntity> extends AbstractService<T>
        implements IBusinessEntityService<T> {

    @NotNull
    private final IBusinessEntityRepository<T> repository;

    public AbstractBusinessEntityService(@NotNull final IBusinessEntityRepository<T> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    @SneakyThrows
    public int size(@NotNull final String userId) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        return repository.size(userId);
    }

    @Override
    @SneakyThrows
    public boolean isEmpty(@NotNull final String userId) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        return repository.isEmpty(userId);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    @SneakyThrows
    public int indexOf(@NotNull final String userId, @NotNull final T entity) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(entity).orElseThrow(EmptyEntityException::new);
        return repository.indexOf(userId, entity);
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> findAll(@NotNull final String userId) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        return repository.findAll(userId);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T findById(@NotNull final String userId, @NotNull final String id) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(id).orElseThrow(EmptyIdException::new);
        return Optional.ofNullable(repository.findById(userId, id))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T findByIndex(@NotNull final String userId, int index) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        index = index - 1;
        if (isIndexIncorrect(index, size(userId))) throw new IncorrectIndexException(index);
        return Optional.ofNullable(repository.findByIndex(userId, index))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T findByName(@NotNull final String userId, @NotNull final String name) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(name).orElseThrow(EmptyNameException::new);
        return Optional.ofNullable(repository.findByName(userId, name))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T editById(
            @NotNull final String userId, @NotNull final String id,
            @NotNull final String name, @NotNull String description
    ) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(id).orElseThrow(EmptyIdException::new);
        Optional.of(name).orElseThrow(EmptyNameException::new);
        description = Optional.of(description).orElse(DEFAULT_DESCRIPTION);
        return Optional.ofNullable(repository.editById(userId, id, name, description))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T editByIndex(
            @NotNull final String userId, int index,
            @NotNull final String name, @NotNull String description
    ) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        --index;
        if (isIndexIncorrect(index, size(userId))) throw new IncorrectIndexException(index);
        Optional.of(name).orElseThrow(EmptyNameException::new);
        description = Optional.of(description).orElse(DEFAULT_DESCRIPTION);
        return Optional.ofNullable(repository.editByIndex(userId, index, name, description))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T editByName(
            @NotNull final String userId, @NotNull final String name,
            @NotNull String description
    ) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(name).orElseThrow(EmptyNameException::new);
        description = Optional.of(description).orElse(DEFAULT_DESCRIPTION);
        return Optional.ofNullable(repository.editByName(userId, name, description))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        repository.clear(userId);
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(id).orElseThrow(EmptyIdException::new);
        repository.removeById(userId, id);
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, int index) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        --index;
        if (isIndexIncorrect(index, size(userId))) throw new IncorrectIndexException(index);
        repository.removeByIndex(userId, index);
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(name).orElseThrow(EmptyNameException::new);
        repository.removeByName(userId, name);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T startById(@NotNull final String userId, @NotNull final String id) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(id).orElseThrow(EmptyIdException::new);
        return Optional.ofNullable(repository.startById(userId, id))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T endById(@NotNull final String userId, @NotNull final String id) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(id).orElseThrow(EmptyIdException::new);
        return Optional.ofNullable(repository.endById(userId, id))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @NotNull
    @SneakyThrows
    @SuppressWarnings("ConstantConditions")
    public List<T> sortByName(@NotNull final String userId) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        @NotNull val entities = repository.findAll(userId);
        entities.sort((first, second) -> {
            @Nullable val firstName = first.getName();
            @Nullable val secondName = second.getName();
            if (anyNull(firstName, secondName)) return 0;
            return firstName.compareTo(secondName);
        });
        return entities;
    }

    @Override
    @NotNull
    @SneakyThrows
    @SuppressWarnings("ConstantConditions")
    public List<T> sortByStartDate(@NotNull final String userId) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        @NotNull val entities = repository.findAll(userId);
        entities.sort((first, second) -> {
            @Nullable val firstStartDate = first.getStartDate();
            @Nullable val secondStartDate = second.getStartDate();
            if (anyNull(firstStartDate, secondStartDate)) return 0;
            return firstStartDate.compareTo(secondStartDate);
        });
        return entities;
    }

    @Override
    @NotNull
    @SneakyThrows
    @SuppressWarnings("ConstantConditions")
    public List<T> sortByEndDate(@NotNull final String userId) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        @NotNull val entities = repository.findAll(userId);
        entities.sort((first, second) -> {
            @Nullable val firstEndDate = first.getEndDate();
            @Nullable val secondEndDate = second.getEndDate();
            if (anyNull(firstEndDate, secondEndDate)) return 0;
            return firstEndDate.compareTo(secondEndDate);
        });
        return entities;
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByStatus(@NotNull final String userId) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        @NotNull val entities = repository.findAll(userId);
        entities.sort((first, second) -> {
            @NotNull val firstStatus = first.getStatus();
            @NotNull val secondStatus = second.getStatus();
            return firstStatus.compareTo(secondStatus);
        });
        return entities;
    }

}
