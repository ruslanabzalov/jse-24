package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.IRepository;
import tsc.abzalov.tm.api.IService;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.model.AbstractEntity;

import java.util.List;
import java.util.Optional;

@SuppressWarnings("ResultOfMethodCallIgnored")
public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @NotNull
    private final IRepository<T> repository;

    public AbstractService(@NotNull final IRepository<T> repository) {
        this.repository = repository;
    }

    @Override
    public long size() {
        return repository.size();
    }

    @Override
    public boolean isEmpty() {
        return repository.isEmpty();
    }

    @Override
    public void create(@NotNull final T entity) {
        repository.create(entity);
    }

    @Override
    @NotNull
    public List<T> findAll() {
        return repository.findAll();
    }

    @Override
    @Nullable
    @SneakyThrows
    public T findById(@NotNull final String id) {
        Optional.of(id).orElseThrow(EmptyIdException::new);
        return repository.findById(id);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String id) {
        Optional.of(id).orElseThrow(EmptyIdException::new);
        repository.removeById(id);
    }

}
